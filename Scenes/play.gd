extends Node2D

var buttons = []
var button = preload("res://Game Objects/light.tscn")
var player = 0
var play_wh = Vector2(5,6)
var pattern = 0
var score = []
var max_turn = 10

var patterns = []
var avaliable_patterns = []

func _ready():
	randomize()
	set_process(true)
	
	max_turn = get_node("/root/globals").get_rounds()
	
	score.push_back(0)
	score.push_back(0)
	
	patterns.push_back([Vector2(0,1),Vector2(-1,0),Vector2(0,-1),Vector2(1,0)])
	patterns.push_back([Vector2(0,0),Vector2(-1,0),Vector2(0,-1),Vector2(1,0)])
	patterns.push_back([Vector2(0,1),Vector2(0,0),Vector2(0,-1),Vector2(1,0)])
	patterns.push_back([Vector2(0,1),Vector2(-1,0),Vector2(0,-1),Vector2(0,0)])
	patterns.push_back([Vector2(0,1),Vector2(-1,0),Vector2(0,0),Vector2(1,0)])
	patterns.push_back([Vector2(0,1),Vector2(1,1),Vector2(0,0),Vector2(1,0)])
	
	for i in range(0,patterns.size(),1):
		avaliable_patterns.push_back(i)
	
	for x in range(play_wh.x):
		buttons.append([])
		buttons[x]=[]
		for y in range(play_wh.y):
			buttons[x].append([])
			buttons[x][y]=button.instance()
			buttons[x][y].loc = Vector2(x,y)
			buttons[x][y].set_pos(Vector2((x * 122.5) + 19.5, (y * 122.5) + 19.5))
			buttons[x][y].connect("set",self,"next_turn")
			add_child(buttons[x][y])
	set_button_images()
	get_tree().set_pause(true)

func _process(delta):
	
	get_scores()

func get_scores():
	score[0] = 0
	score[1] = 0
	for x in range(play_wh.x):
		for y in range(play_wh.y):
			if buttons[x][y].color -1 >= 0:
				score[buttons[x][y].color -1] += 1
				
	var player_text = get_node("lbl_player_turn")
	var score_text = get_node("lbl_score")
	var turn_text = get_node("lbl_turn_count")
	
	turn_text.set_text("Turns left " + str(ceil(max_turn)))
	
	score_text.set_text("White = " + str(score[0]) + "\n Black = " + str(score[1]))
	
	if player == 0:
		player_text.set_text("White Turn")
	else:
		player_text.set_text("Black Turn")

func next_turn():
	player += 1
	max_turn -= 0.5
	if player > 1:
		player = 0
		
	set_button_images()
	
	get_scores()
	
	if max_turn <= 0:
		var winn = "Tie"
		if score[0] > score[1]:
			winn = "White"
		elif score[0] < score[1]:
			winn = "Black"
		get_node("/root/globals").set_winner(winn)
		get_node("/root/globals").setScene("res://Scenes/win.tscn")
	
	get_tree().set_pause(true)
	
func set_button_images():
	for pattern_button in get_tree().get_nodes_in_group("pattern button"):
		var t_pattern = randi() % patterns.size()
		pattern_button.my_pattern = t_pattern
	
func randomize_array(array):
	randomize()
	var new_array = []
	for item in array:
		var rand = randi() % array.size()
		new_array.push_back(array[rand])
		array.remove(rand)
	return new_array
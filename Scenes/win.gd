
extends Node2D

var winner

func _ready():
	get_tree().set_pause(false)
	winner = get_node("/root/globals").get_winner()
	get_node("Label").set_text(str(winner, " WINS"))


func _on_Button_pressed():
	get_tree().set_pause(false)
	get_node("/root/globals").setScene("res://Scenes/game.tscn")

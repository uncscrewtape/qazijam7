# Black and Lights #
A 2 player puzzle game made in [Godot](https://godotengine.org/) by Nate Lewis

This game is my entry into [QaziJam7](https://itch.io/jam/qazijam7L). The theme presented for this jam was Black and White.

[Itch.io page](http://ilovetojam.itch.io/black-and-light)

# How to play #
1. Before you start decided which player will be WHITE and which will be BLACK.
1. White player goes first.
1. At the start of each turn you will be presented with 3 tetromino patterns. This pattern is how you will interact with the light squares for that turn.
1. Your pattern you will either activate a light based on your player color or you will swap an already lit light to the opposite color.
1. After 10 rounds whoever has the most lights wins.

# TO DO #
* Add game options to main menu
* Add more tetromino patterns
* Fix pattern button hitboxes
* Change alpha of pattern buttons
* ~~Add win screen.~~
* ~~Add game win scenario~~
* ~~Fix resolution issue~~

# Contact #
Send any contact requests to nate@ilovetojam.com
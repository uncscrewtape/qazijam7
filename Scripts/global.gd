extends Node

var currentscene = null
var rounds = 10
var winner = "Tie"

func get_winner():
	return winner
	
func set_winner(win):
	winner = win

func get_rounds():
	return rounds

func get_number_rounds():
	return rounds

func _ready():
	currentscene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)
	
func setScene(scene):
	currentscene.queue_free()
	var s = load(scene)
	currentscene = s.instance()
	get_tree().get_root().add_child(currentscene)
extends Sprite

func _ready():
	set_process(true)
	set_process_input(true)

func _process(delta):
	set_hidden(!get_tree().is_paused())
	var turn = get_tree().get_root().get_node("play").player
	var text = ""
	if turn == 0:
		text = "White Player's\nTurn"
	else:
		text = "Black Player's\nTurn"
	get_node("Label").set_text(text)

func _input(event):
	#if event.is_action_released("mouse_buttons"):
		#get_tree().set_pause(false)
	pass

func _on_pattern1_pressed():
	get_tree().get_root().get_node("play").pattern = 0
	get_tree().set_pause(false)

func _on_pattern2_pressed():
	get_tree().get_root().get_node("play").pattern = 1
	get_tree().set_pause(false)

func _on_pattern3_pressed():
	get_tree().get_root().get_node("play").pattern = 1
	get_tree().set_pause(false)
extends TextureButton

var color = 0
var loc = Vector2(0,0)
var main_node
var colors = []
var color_changed = false

signal set

func _ready():
	main_node = get_tree().get_root().get_node("play")
	colors = [get_modulate(), Color(255,255,255), Color(0,0,0)]
	color = 0
	set_process(true)
	
func _process(delta):
	if(!color_changed):
		set_modulate(colors[color])
	
	if is_hovered():
		for target_loc in main_node.patterns[main_node.pattern]:
			var new_loc = target_loc + loc
			if new_loc.x >= 0 && new_loc.y >= 0 && new_loc.x < main_node.play_wh.x && new_loc.y < main_node.play_wh.y:
				var target_button = main_node.buttons[new_loc.x][new_loc.y]
				var target_color = target_button.color
				var set_color = main_node.player + 1
				
				if target_color == 1:
					set_color = 2
				if target_color == 2:
					set_color = 1
				
				target_button.my_set_modulate(colors[set_color])
	
	color_changed = false
	
func my_set_modulate(val):
	color_changed = true
	set_modulate(val)

func _on_light_pressed():
	for target_loc in main_node.patterns[main_node.pattern]:
		var new_loc = target_loc + loc
		if new_loc.x >= 0 && new_loc.y >= 0 && new_loc.x < main_node.play_wh.x && new_loc.y < main_node.play_wh.y:
			var target_button = main_node.buttons[new_loc.x][new_loc.y]
			var target_color = target_button.color
			var set_color = main_node.player + 1
			
			if target_color == 1:
				set_color = 2
			if target_color == 2:
				set_color = 1
			
			target_button.color = set_color
	main_node.next_turn()

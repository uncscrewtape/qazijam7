
extends Button

var my_pattern = 0
var main_node

func _ready():
	set_process(true)
	main_node = get_tree().get_root().get_node("play")
	
func _process(delta):
	get_node("Sprite").set_texture(load("res://Raw Assets/" + str(my_pattern) + ".png"))

func _on_Button_pressed():
	main_node.pattern = my_pattern
	get_tree().set_pause(false)